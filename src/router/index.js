import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    component: () => import("./../views/HomeTemplate"),
    children: [
      {
        path: "/",
        component: () => import("./../views/HomeTemplate/HomePage"),
      },
      {
        path: "/submitBDS",
        component: () => import("./../views/HomeTemplate/SubmitBDS"),
      },
      {
        path: "/login",
        component: () => import("./../views/HomeTemplate/LoginAccount"),
      },
      {
        path: "/register",
        component: () => import("./../views/HomeTemplate/RegisterAccount"),
      },
      {
        path: "/search",
        component: () => import("./../views/HomeTemplate/SearchAreaPage"),
      },
      {
        path: "/detail",
        component: () => import("./../views/HomeTemplate/DetailAreaPage"),
      },
      {
        path: "/updateinfo",
        component: () => import("./../views/HomeTemplate/UserInfoUpdate"),
      },
      {
        path: "/userinfo",
        component: () => import("./../views/HomeTemplate/UserInfo"),
      },
      {
        path: "/realestatemanagement",
        component: () => import("./../views/HomeTemplate/BDSManagement"),
      },
      {
        path: "/editrealestatemanagement",
        component: () => import("./../views/HomeTemplate/EditBDSManagement"),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  linkExactActiveClass: "active",
  routes,
});

export default router;
