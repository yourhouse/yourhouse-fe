import fetch from "./fetch";

export const uploadImg = (file) => {
  var formData = new FormData();
  formData.append("file", file);
  return fetch.post("/upload/img", formData);
};
