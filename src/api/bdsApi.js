import fetch from "./fetch";

export const createBDS = (payload) => {
  return fetch.post("/BDS", payload);
};
export const getBDS = (payload) => {
  return fetch.get("/BDS", payload);
};
export const getDefaultHouseSale = (limit) => {
  return fetch.get("/BDS/getDefaultHouseSale", {
    params: { limit },
  });
};
export const getDefaultHouseRent = (limit) => {
  return fetch.get("/BDS/getDefaultHouseRent", {
    params: { limit },
  });
};
export const getDefaultDepartmentSale = (limit) => {
  return fetch.get("/BDS/getDefaultDepartmentSale", {
    params: { limit },
  });
};
export const getDefaultDepartmentRent = (limit) => {
  return fetch.get("/BDS/getDefaultDepartmentRent", {
    params: { limit },
  });
};
export const getDefaultLandSale = (limit) => {
  return fetch.get("/BDS/getDefaultLandSale", {
    params: { limit },
  });
};
export const getTopViewBDS = (limit) => {
  return fetch.get("/BDS/getTopView", {
    params: { limit },
  });
};
