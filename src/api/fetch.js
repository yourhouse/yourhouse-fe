import axios from "axios";

const fetch = axios.create({
  baseURL: process.env.VUE_APP_HOST_API || "http://localhost:3000",
  headers: {
    Authorization:
      "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MDI5MjEzZTgyZTFlZDA2OWVmNzE5YzAiLCJpYXQiOjE2MTMzMDgyMzZ9.HCP6rds4uUPVTA3EGLwJEmn4RfLTVX2Q9S91aKPriY4",
  },
});

export default fetch;
